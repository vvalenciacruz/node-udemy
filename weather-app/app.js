const request = require('request');
const yargs = require('yargs');

const argv = yargs
  .options({
    a: {
      demand: true,
      alias: 'address',
      describe: 'Address to fetch weather for',
      string: true
    }
  })
  .help()
  .alias('help', 'h')
  .argv;

var address = argv.a;
var encodedAddress = encodeURIComponent(address);

request({
  url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=${process.env.WEATHER_APP_KEY}`,
  json: true
}, (error, response, body) => {
  var result = body.results[0];
  var location = result.geometry.location;
  console.log(`Address: ${result.formatted_address}`);
  console.log(`Latitude: ${location.lat}, Longitude: ${location.lng}`);
});
