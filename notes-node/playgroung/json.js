var obj = {
  name: 'Victor'
};

var stringObj = JSON.stringify(obj);

console.log(typeof stringObj);
console.log(stringObj);

var personString = '{"name": "Victor", "age": 36}';
var person = JSON.parse(personString);

console.log(typeof person);
console.log(person);

console.log('-----File-----');

const filename = 'notes.json';
const fs = require('fs');

var originalNote = {
  title: 'Some title',
  body: 'Some body'
};

var originalNoteString = JSON.stringify(originalNote);

fs.writeFileSync(filename, originalNoteString);

var noteString = fs.readFileSync(filename);
var note = JSON.parse(noteString);

console.log(typeof note);
console.log(note);
console.log(`Note title: ${note.title}`);
